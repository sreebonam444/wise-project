STUDENT ATTENDANCE APPROVAL-Effortless approval of student attendance

This was our first full python project completed during the wise session.The idea of our project came from the issue we face for seeking attendance permission during crucial period.To overcome this problem we came with this idea.The core interface is provided by the main.py file,offering GUI to interact with the system.We used Python3.12.0 for coding. It's important to note that the system relies on maintaining text files in the same location as the Python files for proper functionality. widgets such as buttons,entry boxes etc.. are used in this project.

The goal of student attendance approval is it provide convinience for both teachers and students.And users can access this anywhere anytime with an internet connection.Lets go into the projecct in detail to understand its working in both ways that is according to the student and the admin.
Before going into project we need to make sure that we have database for student and admin details to ensure the efficient working of the project.

The starting page of our project contains login details of student and the admin.after successfully entering the credentials it will lead us to the corresponding connect page.
STUDENT DASHBOARD:
After entering the student details we will be directed to student dashboard which contains student profile and two options namely 
1)make request
2)view request
In make request page it will ask u some details like ID, Name, Branch, From Date, To Date, Periods and Reason for seeking permission.After clicking on the enter button your response will be submitted to the admin.
In view  request page the student can check the status of their request.
ADMIN DASHBOARD:
After entering the admin details we will be directed into admin dashboard wchch contains admin profile and two options
1)Student requests
2)Student details
In student requests it contains the requests posted by the student so that admin can either accept or deny the request based on the reason the submitted.After accepting or declining the request the status can be shown to the student.

Conclusion : 
The project is useful and convinent for both student and teacher. By using python and sql technologies it provides an user friendly platform,facilitating communication between staff and students which is effective.with its interfaces and comprehensive feautures the system provides efficiency.
